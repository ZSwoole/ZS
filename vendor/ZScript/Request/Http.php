<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/27
 * Time: 19:09
 */

namespace ZScript\Request;

use ZScript\APP;

class Http
{
    private static $_request;
    private static $_get;
    private static $_post;
    private static $_server;
    private static $_header;

    public static function addRequest($data)
    {
        self::$_request = array_merge(self::$_request, $data);
    }

    /**
     * ------------------------------------------------------------------
     * 获取request
     * @return mixed
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getRequest()
    {
        return self::$_request;
    }

    public static function setRequest(\swoole_http_request $request)
    {
        self::$_get = $request->get;
        self::$_post = $request->post;
        self::$_server = $request->server;
        self::$_header = $request->header;

        self::$_request = array_merge(($request->get ?? array()), ($request->post ?? array()));
    }

    /**
     * ------------------------------------------------------------------
     * 销毁当前request的赋值
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function destroy()
    {
        self::$_request = null;
        self::$_get = null;
        self::$_post = null;
        self::$_server = null;
        self::$_header = null;

        // TODO: 注销全局静态变量
        APP::unStaticVar();
    }

    /**
     * @return mixed
     */
    public static function getGet()
    {
        return self::$_get;
    }

    /**
     * @return mixed
     */
    public static function getPost()
    {
        return self::$_post;
    }

    /**
     * @return mixed
     */
    public static function getServer()
    {
        return self::$_server;
    }

    /**
     * @return mixed
     */
    public static function getHeader()
    {
        return self::$_header;
    }

    /**
     * Fetches one or more items from a global, like cookies, get, post, etc.
     * Can optionally filter the input when you retrieve it by passing in
     * a filter.
     *
     * If $type is an array, it must conform to the input allowed by the
     * filter_input_array method.
     *
     * http://php.net/manual/en/filter.filters.sanitize.php
     *
     * @param int $type Input filter constant
     * @param string|array $index
     * @param int $filter Filter constant
     * @param null $flags
     *
     * @return mixed
     */
    protected function fetchGlobal($type, $index = null, $filter = null, $flags = null)
    {
        // Null filters cause null values to return.
        if (is_null($filter)) {
            $filter = FILTER_DEFAULT;
        }

        $loopThrough = [];
        switch ($type) {
            case INPUT_GET :
                $loopThrough = $_GET;
                break;
            case INPUT_POST :
                $loopThrough = $_POST;
                break;
            case INPUT_COOKIE :
                $loopThrough = $_COOKIE;
                break;
            case INPUT_SERVER :
                $loopThrough = $_SERVER;
                break;
            case INPUT_ENV :
                $loopThrough = $_ENV;
                break;
            case INPUT_REQUEST :
                $loopThrough = $_REQUEST;
                break;
        }

        // If $index is null, it means that the whole input type array is requested
        if (is_null($index)) {
            $values = [];
            foreach ($loopThrough as $key => $value) {
                $values[$key] = is_array($value) ? $this->fetchGlobal($type, $key, $filter, $flags) : filter_var($value, $filter, $flags);
            }

            return $values;
        }

        // allow fetching multiple keys at once
        if (is_array($index)) {
            $output = [];

            foreach ($index as $key) {
                $output[$key] = $this->fetchGlobal($type, $key, $filter, $flags);
            }

            return $output;
        }

        // Does the index contain array notation?
        if (($count = preg_match_all('/(?:^[^\[]+)|\[[^]]*\]/', $index, $matches)) > 1) {
            $value = $loopThrough;
            for ($i = 0; $i < $count; $i++) {
                $key = trim($matches[0][$i], '[]');

                if ($key === '') // Empty notation will return the value as array
                {
                    break;
                }

                if (isset($value[$key])) {
                    $value = $value[$key];
                } else {
                    return null;
                }
            }
        }

        // Due to issues with FastCGI and testing,
        // we need to do these all manually instead
        // of the simpler filter_input();
        if (empty($value)) {
            $value = $loopThrough[$index] ?? null;
        }

        if (is_array($value) || is_object($value) || is_null($value)) {
            return $value;
        }

        return filter_var($value, $filter, $flags);
    }

    //--------------------------------------------------------------------
}