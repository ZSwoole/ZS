<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2018/12/5
 * Time: 13:49
 * Desc:
 */

namespace ZScript\Request;

namespace ZScript\Request;

use ZScript\APP;

class Tcp
{
    private static $_request;

    /**
     * @return mixed
     * ------------------------------------------------------------
     */
    public static function getRequest()
    {
        return self::$_request;
    }

    /**
     * @param mixed $request
     * ------------------------------------------------------------
     */
    public static function setRequest($request)
    {
        self::$_request = $request;
    }

    /**
     * ------------------------------------------------------------------
     * 销毁当前request的赋值
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function destroy()
    {
        self::$_request = null;

        // TODO: 注销全局静态变量
        APP::unStaticVar();
    }
}