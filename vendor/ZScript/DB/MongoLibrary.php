<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2019/3/1
 * Time: 2:36 PM
 */

namespace ZScript\DB;


class MongoLibrary {

    static $mongo;
    static $db;
    static $collections;


    /**
     * MongoLibrary constructor.
     * @param $params
     * @return MongoLibrary
     */
    public function __construct($params) {

        self::$db = $params['db'];
        #下行代码为第三方类库
        self::$mongo = new \MongoDB\Driver\Manager("mongodb://".$params['username'].':'.$params['password'].'@'.$params['host'].':'.$params['port']);

    }


    /**
     * ------------------------------------------------------------------
     * 返回表
     * @param $collection
     * @return \MongoDB\Collection
     * @author soosoogoo
     * ------------------------------------------------------------------\
     */
    public static function instance($collection)
    {
        if(self::$collections[$collection])
        {
            return self::$collections[$collection];
        }
        #建立连接  mongodb://username:password@host:port
        return  new \MongoDB\Collection(self::$mongo, self::$db, $collection);
    }

}