<?php
/*
 * 此处旨在减少数据库多余链接，充分利用已有链接，并且最小化对数据库配置文件的修改
 * @Name SQL链接管理
 * @Author Soosoogoo
 * @version 1.01
 * @var data 2014/5/13
 */


namespace ZScript\DB;


use Config\Databases;

class Links
{
    const MAIN_GROUP = 'main';//主DB分区

    public static $redisPool;
    public static $mysqlPool;
    public static $mongoPool;
    public static $adsPool;

    public static $mysqlLog;
    private static $beginTimeList = [];

    /**
     * redis管理
     * @param string $active_group
     * @param bool $isChildren
     * @return \Redis
     */
    public static function getRedis($active_group = self::MAIN_GROUP, $isChildren = false)
    {
        //如若已经有缓存
        if (isset(self::$redisPool[$active_group]) && !$isChildren) {
            return self::$redisPool[$active_group];
        }

        //如果没传参数则尝试从配置文件获取
        $params = Databases::$redis[$active_group];

        self::$redisPool[$active_group] = new RedisLibrary($params);
        return self::$redisPool[$active_group];
    }

    /**
     * 加载数据库连接模型
     * @param    string    the DB credentials
     * @param    bool    whether to return the DB object
     * @return MysqliDb
     */
    public static function getMysql($active_group = self::MAIN_GROUP)
    {
        // 判断当期实例是否存在 效验mysql连接实例是否在两小时内 超过两小时则重新加载mysql 实例
        if (isset(self::$mysqlPool[$active_group]) && self::checkMysqlConnet($active_group)) {

            return self::$mysqlPool[$active_group];
        }

        $params = Databases::$mysql[$active_group];

        self::$mysqlPool[$active_group] = new MysqliDb($params);

        return self::$mysqlPool[$active_group];
    }


    /**
     * 加载数据库连接模型
     * @param    string    the DB credentials
     * @param    bool    whether to return the DB object
     * @return MysqliDb
     */
    public static function getAds($active_group = self::MAIN_GROUP)
    {
        // 判断当期实例是否存在 效验mysql连接实例是否在两小时内 超过两小时则重新加载mysql 实例
        if (isset(self::$adsPool[$active_group]) && self::checkMysqlConnet($active_group)) {

            return self::$adsPool[$active_group];
        }

        $params = Databases::$ads[$active_group];

        self::$adsPool[$active_group] = new MysqliDb($params);

        return self::$adsPool[$active_group];
    }



    /**
     * 加载数据库连接模型
     * @param    string    the DB credentials
     * @param    bool    whether to return the DB object
     * @return MongoLibrary
     */
    public static function getMongo($active_group = self::MAIN_GROUP)
    {
        // 判断当期实例是否存在 效验mysql连接实例是否在两小时内 超过两小时则重新加载mysql 实例
        if (isset(self::$mongoPool[$active_group])) {

            return self::$mongoPool[$active_group];
        }

        $params = Databases::$mongo[$active_group];

        self::$mongoPool[$active_group] = new MongoLibrary($params);

        return self::$mongoPool[$active_group];
    }

    /**
     * @desc 效验mysql连接实例是否超过2小时
     * @return bool
     * ------------------------------------------------------------
     */
    public static function checkMysqlConnet($key)
    {
        $begin_time = self::$beginTimeList[$key] ?? 0;

        // 每次连接mysql 更新创建实例时间 (防止超过2小时未连接mysql 中断问题 同时修复 事务不一致问题)
        self::setBeginTimeList($key);

        return $begin_time > time() - 7200;
    }

    /**
     * @param array $beginTimeList
     * ------------------------------------------------------------
     */
    public static function setBeginTimeList($key)
    {
        self::$beginTimeList[$key] = time();
    }

}
