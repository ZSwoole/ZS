<?php

namespace ZScript\DB;

use ZScript\APP;

class RedisLibrary
{
    public $prefix_key = '';
    public $_session_key = 'PHPREDIS_SESSION:';
    private $sredis;
    private $params;

    public function __construct($params)
    {
        $this->redisLoad($params);
    }

    /**
     * @desc 加载Redis
     * @param $params
     * ------------------------------------------------------------
     */
    public function redisLoad($params)
    {
        try {
            $this->sredis = new \Redis();
            $this->prefix_key = $params['prefix'] ?? '';
            $this->sredis->connect($params['host'], $params['port'], 1);
            $this->params = $params;
            if ($params['database']) {
                $this->sredis->select($params['database']);
            }
            if (isset($params['pwd']) && $params['pwd']) {

                $this->sredis->auth($params['pwd']);
            }
            return true;
        } catch (\Exception $e) {
            sleep(1);
            echo 'redis 连接断开重新加载中',PHP_EOL;
            APP::log(['msg' => 'redis 连接断开重新加载中', 'params' => [$params['host'], $params['port']]]);
            $result = $this->redisLoad($params);

            return $result;
        }
    }

    /**
     * @param $method
     * @param array $params
     * @return mixed
     * ------------------------------------------------------------
     */
    public function __call($method, $params = array())
    {
        if (isset($params[0]) && !in_array($method, ['setOption', 'subscribe', 'psubscribe'])) {
            $params[0] = $this->prefix_key . $params[0];
        }

        try {
            return call_user_func_array(array($this->sredis, $method), $params);
        } catch (\Exception $e) {
            if ($this->redisLoad($this->params)) {
                return call_user_func_array(array($this->sredis, $method), $params);
            }
        }
    }

}