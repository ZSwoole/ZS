<?php

/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/27
 * Time: 17:28
 */

namespace ZScript;

use Config\Main;
use ZScript\Server\Server;

class ZScript
{
    /**
     * @var ZScript
     */
    protected static $instance;
    private static $classPath = array();

    /**
     * @param callable|null $preCall
     * @return ZScript
     * ------------------------------------------------------------
     */
    static function getInstance(callable $preCall = null)
    {
        if (!isset(self::$instance)) {
            self::$instance = new self($preCall);
        }
        return self::$instance;
    }

    /**
     * 初始化目录
     * @author soosoogoo
     */
    public static function initWork()
    {
        //自动加载
        \spl_autoload_register(__CLASS__ . '::autoLoader');
    }

    /**
     * @param $class
     * @desc 自动加载类,
     * ps当高并发情况下，is_file可能会导致cpu打满，可以取消掉is_file判断, swoole模式无此问题
     */
    final public static function autoLoader($class)
    {
        if (isset(self::$classPath[$class])) {
            return;
        }
        $loadClasspath = \str_replace('\\', DS, $class) . '.php';

        //设置目录
        $libs = array(
            SRC_PATH,//src目录
            \dirname(__DIR__),//vendor 目录
            BIN_PATH
        );

        //遍历查找文件
        foreach ($libs as $lib) {
            $classpath = $lib . DS . $loadClasspath;
            if (\is_file($classpath)) {
                require "{$classpath}";
                self::$classPath[$class] = $classpath;
                return;
            }
        }

    }

    /**
     * ------------------------------------------------------------------
     * 启动
     * @author soosoogoo
     * @throws
     * ------------------------------------------------------------------
     */
    public static function run()
    {
        //获取实例
        (new Server())->startServer();
    }

    /**
     * 重启
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function reload()
    {
        $pid = file_get_contents(LOG_PATH . DS . 'pid');
        if (!\swoole_process::kill($pid, 0)) {
            echo "pid :{$pid} not exist ", PHP_EOL;
            return;
        }

        \swoole_process::kill($pid, SIGUSR1);//all - $sig = SIGUSR2;$sig = SIGUSR1;

        echo($pid . " - send server reload command at "), PHP_EOL;
    }

    /**
     * ------------------------------------------------------------------
     * 重新启动所有work
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function restartAllWork()
    {
        $pids = file_get_contents(LOG_PATH . DS . 'workPids');
        $pids = explode(',', trim($pids, ','));

        foreach ($pids as $pid) {
            if (!\swoole_process::kill($pid, 0)) {
                APP::log("pid :{$pid} not exist ");
                return;
            }
            //echo $pid, PHP_EOL;
            \swoole_process::kill($pid, SIGUSR1);//all - $sig = SIGUSR2;$sig = SIGUSR1;
            APP::log($pid . " - send server reload command at ");
        }
        APP::log('restart all process');
    }


    /**
     * ------------------------------------------------------------------
     * 命令符
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function commandParser()
    {
        global $argv;
        $command = '';
        $options = array();
        if (isset($argv[1])) {
            $command = $argv[1];
        }
        $argv = $argv ? $argv : array();
        foreach ($argv as $item) {
            if (substr($item, 0, 2) === '--') {
                $temp = trim($item, "--");
                $temp = explode("-", $temp);
                $key = array_shift($temp);
                $options[$key] = array_shift($temp) ?: '';
            }
        }
        return array(
            "command" => $command,
            "options" => $options
        );
    }

    /**
     * ------------------------------------------------------------------
     * 合并配置文件
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function mergeConfig($command)
    {
        if (!empty($command['options']['conf'])) {
            $configClass = 'BinConfig\\' . $command['options']['conf'];
            if (class_exists($configClass)) {
                $conf = $configClass::config();
                //进程名称
                if (isset($conf['projectName'])) {
                    Main::$projectName = $conf['projectName'];
                }
                //业务名称
                if (isset($conf['projectExplain'])) {
                    Main::$projectExplain = $conf['projectExplain'];
                }
                //swoole基础配置
                if (isset($conf['swooleSettins'])) {
                    Main::$swooleSettins = array_merge(Main::$swooleSettins, $conf['swooleSettins']);
                }
                //业务逻辑
                if (isset($conf['autoRoute'])) {

                    Main::$autoRoute = $conf['autoRoute'];
                }

                //分发器
                if (isset($conf['dispense'])) {

                    Main::$dispense = $conf['dispense'];
                }

                //子进程
                if (isset($conf['pipProcess'])) {
                    Main::$pipProcess = array_merge(Main::$pipProcess, $conf['pipProcess']);
                }

                //主逻辑配置
                if (isset($conf['serviceConfig'])) {
                    Main::$serviceConfig = $conf['serviceConfig'];
                }

            } else {
                echo "配置文件不存在" . PHP_EOL;
                exit;
            }
        } else {
            echo "请带上配置文件" . PHP_EOL;
            exit;

        }
    }


    /**
     * ------------------------------------------------------------------
     * 控制面板输出
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function showConfig()
    {
        echo PHP_EOL . "  启动成功" . PHP_EOL;
        echo "  监听端口 : " . Main::$swooleSettins['host'] . ":" . Main::$swooleSettins['port'] . PHP_EOL;
        echo "  进程名称 : " . Main::$projectName . PHP_EOL;
        echo "  业务名称 : " . Main::$projectExplain . PHP_EOL;
        echo "  进程数量 : " . Main::$swooleSettins['worker_num'] . PHP_EOL . PHP_EOL;
    }

    /**
     * @desc cli 运行模式
     * @param array $params
     * ------------------------------------------------------------
     */
    public static function cliRun(array $params)
    {
        unset($params[0]);
        if (empty($params)) {
            echo '无有效指令', PHP_EOL;
            return;
        }

        (new \ZScript\Cli\Server())->startServer(array_values($params));
    }
}