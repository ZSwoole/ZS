<?php
namespace ZScript\Core;
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 6/19
 * Time: 23:03
 */

class Url
{

    public static function pathInfo($path)
    {
        $basePath = dirname($path);
        $info = pathInfo($path);
        if($info['filename'] != 'index'){
            if($basePath == '/'){
                $basePath = $basePath.$info['filename'];
            }else{
                $basePath = $basePath.'/'.$info['filename'];
            }
        }
        return $basePath;
    }
}

