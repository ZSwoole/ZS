<?php

namespace ZScript\PipProcess;

use Config\Main;
use Library\Core\Config;
use ZScript\Process\Process as ZScriptProcess;

/**
 * Class Dispense
 * @package PipProcess
 */
class Dispense extends ZScriptProcess
{
    public static $i = 0;

    public function __construct($name, $worker_id)
    {
        parent::__construct($name, $worker_id);
        //echo "child process workId is ".$worker_id ."\n";
        //echo "child process name is ".$name ."\n";

        #注意不能在此处new redis 或者 mysql
    }

    /**
     * ------------------------------------------------------------------
     * 进程开启后的回调
     * @param \swoole_process $process
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public function onStart(\swoole_process $process)
    {
        $process->name(Main::$projectName . "-sws-swoole-fork-process");//设置进程名称

        self::start();
    }

    /**
     * ------------------------------------------------------------------
     * 启动子进程,分配任务
     * 此处要在main处实现 jobs 方法
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function start()
    {
        $taskList = Main::autoRoute()->taskDispense();

        $i = 0;
        foreach ($taskList as $job) {
            list($key, $class, $params, $config) = $job;

            $workId = $i++ % (Main::$swooleSettins['worker_num'] - 1);

            //分配到各个进程
            Config::saveWorkConfig($workId, $job);
        }
    }


    /**
     * ------------------------------------------------------------------
     * 关闭回调
     * @return mixed|void
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public function onShutDown()
    {
        //必须为false，非阻塞模式
        while ($ret = \swoole_process::wait(false)) {
            echo "\n\n\n\n\n\n\n\n\n  -------------------onShutDown-------------------------- \n\n\n\n\n\n\n\n\n";
        }
        // TODO: Implement onShutDown() method.
    }

}