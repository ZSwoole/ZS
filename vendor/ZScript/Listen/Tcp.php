<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2/1
 * Time: 14:14
 */

namespace ZScript\Listen;

abstract class Tcp extends Base
{
    abstract function onReceive(\swoole_server $server, int $fd, int $reactor_id, string $data);
}