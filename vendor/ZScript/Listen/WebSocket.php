<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/5
 * Time: 16:31
 */
namespace ZScript\Listen;

abstract class WebSocket extends Base
{

    abstract function onMessage(\swoole_server  $server,\swoole_websocket_frame $fram);

    abstract function onOpen(\swoole_websocket_server $svr, \swoole_http_request $req);

    //public function onHandShake($request,  $response){}


    public function onClose(\swoole_server $server, int $fd, int $reactorId){}

}
