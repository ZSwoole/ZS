<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/25
 * Time: 15:32
 */

namespace ZScript\Listen;

use Config\Main;
use ZScript\Process\ProcessManager;

class Base
{
    /*
    onStart
    onFinish

    onManagerStart
    onManagerStop

    onWorkerStart
    onWorkerStop
    onWorkerError

    onConnect
    onClose

    onManagerStart
    onManagerStop

    onTask
    onReceive
    onMessage
    onRequest

    onHandShake
         function onHandShake(swoole_http_request $request, swoole_http_response $response);
         https://wiki.swoole.com/wiki/page/409.html

    onPipeMessage
         void onPipeMessage(swoole_server $server, int $src_worker_id, mixed $message);
         https://wiki.swoole.com/wiki/page/366.html

    onPacket
         function onPacket(swoole_server $server, string $data, array $client_info);
         https://wiki.swoole.com/wiki/page/450.html
 */


    /**
     * onSwooleStart
     * @param \swoole_server $serv
     */
    public function onStart(\swoole_server $server)
    {
        ProcessManager::setProcessName(Main::$projectName . '-swoole-main-master');
    }

    public function onShutdown($server)
    {
    }

    /**
     * onSwooleStart
     * @param $serv
     * @param $workerId
     */
    public function onWorkerStart($server, $worker_id)
    {
        //清除缓存
        if (function_exists('apc_clear_cache')) {
            apc_clear_cache();
        }
        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
        // 重新加载配置
        if (!$server->taskworker) {//worker进程
            ProcessManager::setProcessName(Main::$projectName . "-swoole-worker-" . $worker_id);
        } else {
            ProcessManager::setProcessName(Main::$projectName . "-swoole-tasker-" . $worker_id);
        }
    }

    /**
     * ManagerStart
     * @param $serv
     */
    public function onManagerStart($server)
    {
        ProcessManager::setProcessName(Main::$projectName . '-swoole-main-manager');
    }

    /**
     * work结束
     * @param $serv
     * @param $worker_id
     * @author soosoogoo
     */
    public function onWorkerStop($server, $worker_id)
    {
    }

    /**
     * ManagerStop
     * @param $serv
     */
    public function onManagerStop($server)
    {
    }


    //public function onPipeMessage(\swoole_server  $server, int $src_worker_id,  $message){}

    public function onWorkerError()
    {
    }

    public function onTask(\swoole_server $serv, int $task_id, int $src_worker_id, $data)
    {
    }

    public function onFinish(\swoole_server $serv, int $task_id, string $data)
    {
    }

    public function onConnect(\swoole_server $server, int $fd, int $reactorId)
    {
    }

}