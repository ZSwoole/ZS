<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/6
 * Time: 18:10
 */
namespace ZScript\Listen;

abstract class Http extends Base
{

    /**
     * http请求集合
     * @param $request
     * @param $response
     * @return mixed
     * @author soosoogoo
     */
    abstract public function doRequest(\swoole_http_request $request, \swoole_http_response$response);

    /**
     * HTTP请求操作
     * @param $request
     * @param $response
     * @return mixed
     * @author soosoogoo
     */
    abstract public function onRequest(\swoole_http_request $request, \swoole_http_response  $response);


}