<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/5
 * Time: 16:37
 */
namespace ZScript\Server\PortManager;

class PortManager
{

    protected static $callback = [];
    protected static $packs = [];
    protected static $route = [];


    public function __construct($ports)
    {
        self::$callback = $ports;
    }


    /**
     * 构建端口
     * @param Server $swoole_server
     * @param $first_port
     * @throws \Exception
     */
    public function buildPort($swoole_server)
    {
        foreach (self::$callback as $key => $value)
        {
            if ($value['socket_type'] == Server::TYPE_HTTP || $value['socket_type'] == Server::TYPE_WEBSOCKET)
            {
                $client = CFactory::getInstance($value['client_class']);
                $port = $swoole_server->serv->listen($value['socket_host'], $value['port'], SWOOLE_SOCK_TCP );
                if ($port == false) {
                    throw new \Exception("{$value['port']}端口创建失败");
                }
                if ($value['socket_type'] == Server::TYPE_HTTP) {
                    $set['open_http_protocol'] = true;
                    $port->set($set);
                    $port->on('request', [$client,  'doRequest']);
                    $port->on('handshake', function () {
                        return false;
                    });
                } else {
                    $set['open_http_protocol'] = true;
                    $set['open_websocket_protocol'] = true;

                    $port->set($set);
                    $port->on('open', [$client,  'onOpen']);
                    $port->on('message', [$client, 'onMessage']);
                    $port->on('close', [$client,  'onClose']);
                    $port->on('handshake', [$client,  'onHandShake']);
                }

                //执行附加操作
                self::addPortExtr($value);
                Server::log($value['socket_name'] .'start ok , listen as '.$value['socket_host'].':'.$value['port'] );
            }
        }
    }

    /**
     * 执行附加操作
     * @param $conf
     * @author soosoogoo
     */
    public function addPortExtr($conf)
    {
        //添加解析函数
        self::$packs[$conf['port']] = self::createPack($conf['pack_class']);

        //添加路由
        self::$route[$conf['port']] = self::createRoute($conf['route_class']);
    }


    /**
     * @param $pack_tool
     * @return mixed
     * @throws SwooleException
     */
    public static function createPack($pack_tool)
    {
        if (class_exists($pack_tool)) {
            $pack = new $pack_tool;
            return $pack;
        }
        //pack class
        $pack_class_name = "Pack\\" . $pack_tool;
        if (class_exists($pack_class_name)) {
            $pack = new $pack_class_name;
        } else {
            $pack_class_name = "Sws\\Pack\\" . $pack_tool;
            if (class_exists($pack_class_name)) {
                $pack = new $pack_class_name;
            } else {
                throw new \Exception("class $pack_tool is not exist.");
            }
        }
        return $pack;
    }

    /**
     * @param $route_tool
     * @return mixed
     * @throws SwooleException
     */
    public static function createRoute($route_tool)
    {
        if (class_exists($route_tool)) {
            $route = new $route_tool;
            return $route;
        }
        $route_class_name = "Route\\" . $route_tool;
        if (class_exists($route_class_name)) {
            $route = new $route_class_name;
        } else {
            $route_class_name = "Sws\\Server\\Route\\" . $route_tool;
            if (class_exists($route_class_name)) {
                $route = new $route_class_name;
            } else {
                throw new \Exception("class $route_tool is not exist.");
            }
        }
        return $route;
    }


    /**
     * @param $fd
     * @return mixed
     * @author soosoogoo
     */
    public function getRoute($fd)
    {
        $connInfo = Server::getInstance()->serv->connection_info($fd);

        return self::$route[$connInfo['server_port']];
    }

    /**
     * @param $fd
     * @return mixed
     */
    public function getPack($fd)
    {
        $connInfo = Server::getInstance()->serv->connection_info($fd);
        return self::$packs[$connInfo['server_port']];
    }
}