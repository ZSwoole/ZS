<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/2
 * Time: 16:44
 */

namespace ZScript\Server;

use Config\Main;
use ZScript\APP;
use ZScript\Library\SwooleTable;
use ZScript\Process\ProcessManager;

class Server
{
    public $server;
    public $listen;
    public $table;

    /**
     * 构造swoole server.
     * @throws \Exception
     */
    public function __construct()
    {
        if (!\extension_loaded('swoole')) {
            throw new \Exception("no swoole extension. get: https://github.com/swoole/swoole-src");
        }

        switch (Main::$swooleSettins['server_type']) {
            case Main::TYPE_TCP:
                $this->server = new \swoole_server(Main::$swooleSettins['host'], Main::$swooleSettins['port'], SWOOLE_PROCESS, SWOOLE_SOCK_TCP);
                break;
            case Main::TYPE_HTTP:
                $this->server = new \swoole_http_server(Main::$swooleSettins['host'], Main::$swooleSettins['port'], SWOOLE_PROCESS);
                break;
            case Main::TYPE_WEBSOCKET:
                $this->server = new \swoole_websocket_server(Main::$swooleSettins['host'], Main::$swooleSettins['port'], SWOOLE_PROCESS);
                break;
        }

        //设置参数
        $this->server->set(Main::$swooleSettins);

        // 自定义分配work进程规则
        if (!empty(Main::$swooleSettins['open_dispatch_func'])) {
            $this->server->set(array(
                'dispatch_func' => function ($serv, $fd, $type, $packData) {
                    return \Library\Dispatch::mode($packData);
                },
            ));
        }

        $client = 'Listen\\' . Main::$mainMode;
        //设置自定义监听
        $this->listen = new $client();


        //构建端口管理
        //$this->PortManager = new PortManager(Config::get('ports'));

        //添加当前端口
        //$this->PortManager->addPortExtr($this->config);
    }

    /**
     * 启动server
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public function startServer()
    {
        //添加监听
        $this->server->on('Start', [$this->listen, 'onStart']);
        $this->server->on('WorkerStart', [$this->listen, 'onWorkerStart']);
        $this->server->on('ManagerStart', [$this->listen, 'onManagerStart']);

        $this->server->on('Shutdown', [$this->listen, 'onShutdown']);
        $this->server->on('WorkerStop', [$this->listen, 'onWorkerStop']);
        $this->server->on('ManagerStop', [$this->listen, 'onManagerStop']);
        $this->server->on('Close', [$this->listen, 'onClose']);

        $this->server->on('WorkerError', [$this->listen, 'onWorkerError']);

        $this->server->on('Task', [$this->listen, 'onTask']);
        $this->server->on('Finish', [$this->listen, 'onFinish']);

        if (method_exists($this->listen, 'doPipeMessage')) {
            $this->server->on('PipeMessage', [$this->listen, 'doPipeMessage']);
        }

        //进程回调
        switch (Main::$swooleSettins['server_type']) {
            case Main::TYPE_TCP:
                $this->server->on('Connect', array($this->listen, 'onConnect'));
                $this->server->on('Receive', array($this->listen, 'onReceive'));
                break;
            case Main::TYPE_HTTP:
                $this->server->on('Request', array($this->listen, 'onRequest'));
                break;
            case Main::TYPE_WEBSOCKET:
                $this->server->on('Connect', array($this->listen, 'onConnect'));

                if (method_exists($this->listen, 'onHandShake')) {
                    $this->server->on('HandShake', array($this->listen, 'onHandShake'));
                }
                if (method_exists($this->listen, 'onOpen')) {
                    $this->server->on('Open', array($this->listen, 'onOpen'));
                }
                if (method_exists($this->listen, 'doRequest')) {
                    $this->server->on('Request', array($this->listen, 'doRequest'));
                }
                if (method_exists($this->listen, 'onWorkStart')) {
                    $this->server->on('WorkStart', array($this->listen, 'onWorkStart'));
                }

                $this->server->on('Message', array($this->listen, 'onMessage'));
                break;
        }

        //start 开启之前的操作
        self::beforeSwooleStart();

        $this->server->start();
    }


    /**
     * ------------------------------------------------------------------
     * 开启前置操作
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public function beforeSwooleStart()
    {
        //创建uid->fd共享内存表
        $this->table = SwooleTable::getSwooleTable()->createCoinTable();

        // 设置其他调用方式
        APP::setServer($this);

        //构建port监听


        //创建task用的taskid
        $this->task_atomic = new \swoole_atomic(0);

        //开启用户进程
        ProcessManager::getInstance()->startProcess();
    }

}