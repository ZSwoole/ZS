<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2018/9/10
 * Time: 23:03
 * Desc:
 */

namespace ZScript\Router;


class Cli
{
    /**
     * The name of the controller class.
     *
     * @var string
     */
    static $controller;

    /**
     * The name of the method to use.
     *
     * @var string
     */
    static $method;


    /**
     * An array of binds that were collected
     * so they can be sent to closure routes.
     *
     * @var array
     */
    static $params = [];

    static $directory;


    //--------------------------------------------------------------------

    /**
     * Attempts to match a URI path against Controllers and directories
     * found in APPPATH/Controllers, to find a matching route.
     *
     * @param string $uri
     */
    public function autoRoute($segments)
    {
        $segments = $this->validateRequest($segments);

        // If we don't have any segments left - try the default controller;
        // WARNING: Directories get shifted out of the segments array.
        if (empty($segments))
        {
            $this->setDefaultController();
        }
        // If not empty, then the first segment should be the controller
        else
        {
            self::$controller = ucfirst(array_shift($segments));
        }

        // Use the method name if it exists.
        // If it doesn't, no biggie - the default method name
        // has already been set.
        if ( ! empty($segments))
        {
            self::$method = array_shift($segments);
        }

        if ( ! empty($segments))
        {
            self::$params = $segments;
        }

        // Load the file so that it's available for CodeIgniter.
        $file = SRC_PATH . 'Command/' . self::$directory . self::$controller . '.php';
        if (file_exists($file))
        {
            include_once $file;
        }

        // Ensure the controller stores the fully-qualified class name
        // We have to check for a length over 1, since by default it will be '\'
        if (strpos( self::$controller, '\\') === false && strlen('Command\\') > 1)
        {
            self::$controller = str_replace('/', '\\', 'Command\\' . self::$directory . self::$controller);
        }
    }



    //--------------------------------------------------------------------

    /**
     * Attempts to validate the URI request and determine the controller path.
     *
     * @param array $segments URI segments
     *
     * @return array URI segments
     */
    public function validateRequest(array $segments)
    {
        $c = count($segments);
        $directory_override = isset($this->directory);

        // Loop through our segments and return as soon as a controller
        // is found or when such a directory doesn't exist
        while ($c -- > 0)
        {
            $test = ucfirst(str_replace('-', '_', $segments[0]));

            if ( ! file_exists(SRC_PATH . 'Command/' . $test . '.php') && $directory_override === false && is_dir(SRC_PATH . 'Command/' . self::$directory . ucfirst($segments[0]))
            )
            {
                $this->setDirectory(array_shift($segments), true);
                continue;
            }

            return $segments;
        }

        // This means that all segments were actually directories
        return $segments;
    }



    /**
     * Sets the default controller based on the info set in the RouteCollection.
     */
    public function setDefaultController()
    {
        if (empty($this->controller))
        {
            throw new \RuntimeException('Unable to determine what should be displayed. A default route has not been specified in the routing file.');
        }

        // Is the method being specified?
        if (sscanf(self::$controller, '%[^/]/%s', $class, self::$method) !== 2)
        {
            self::$method = 'index';
        }

        if ( ! file_exists(SRC_PATH . 'Command/' . self::$directory . ucfirst($class) . '.php'))
        {
            return;
        }

        self::$controller = ucfirst($class);

    }




    /**
     * Sets the sub-directory that the controller is in.
     *
     * @param string|null $dir
     * @param bool|false  $append
     */
    protected function setDirectory(string $dir = null, $append = false)
    {
        $dir = ucfirst($dir);

        if ($append !== TRUE || empty($this->directory))
        {
            self::$directory = str_replace('.', '', trim($dir, '/')) . '/';
        }
        else
        {
            self::$directory .= str_replace('.', '', trim($dir, '/')) . '/';
        }
    }

    //--------------------------------------------------------------------


    /**
     * Returns the name of the matched controller.
     *
     * @return mixed
     */
    public static function controllerName()
    {
        return self::$controller;
    }

    //--------------------------------------------------------------------

    /**
     * Returns the name of the method to run in the
     * chosen container.
     *
     * @return mixed
     */
    public static function methodName()
    {
        return self::$method;
    }

    //--------------------------------------------------------------------
}