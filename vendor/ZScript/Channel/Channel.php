<?php
namespace ZScript\Channel;
use Config\Main;
use ZScript\APP;
use ZScript\DB\Links;

/**
 * Created by PhpStorm.
 * 此处没有 uid的概念 只有订阅
 * User: soosoogoo
 * Date: 3/13
 * Time: 15:48
 */


class Channel
{
    public static $redis;

    const FD_PREFIX = 'su_fd';
    const CHANNEL_PREFIX = 'su_channel:';

    //注入redis连接
    public function __construct()
    {
        //链接REDIS
        self::$redis = Links::getRedis('channel');
    }


    public static function getKey($prefix,$fd)
    {
        return $prefix.$fd;
    }


    /**
     * 添加fd
     * @param $fd
     * @return mixed
     * @author soosoogoo
     */
    public static function addFd($fd)
    {
        $data = array(
            'fd' => $fd,
            'time' => time(),
            'channels' => array('ALL' => 1)
        );

        $fdInfo = self::getFd($fd);
        if($fdInfo){
            $data = $fdInfo+$data;
        }

        return self::$redis->hset(self::FD_PREFIX,$fd,json_encode($data));
    }


    /**
     * 获取FD信息
     * @param $fd
     * @return  mixed
     * @author soosoogoo
     */
    public static function getFd($fd)
    {
        $affect = self::$redis->hget(self::FD_PREFIX,$fd);
        $affect = $affect ? json_decode($affect,true) : array();
        return $affect;
    }


    /**
     * 修改Fd信息
     * @param $fd
     * @param $data
     * @return mixed
     * @author soosoogoo
     */
    public static function editFd($fd,$data)
    {
        $fdInfo = self::getFd($fd);
        $data = array_merge($fdInfo,$data);

        return self::$redis->hset(self::FD_PREFIX,$fd,json_encode($data));
    }


    /**
     * 删除FD
     * @param $fd
     * @author soosoogoo
     */
    public static function delFd($fd)
    {
        //删除订阅
        $fdInfo = self::getFd($fd);
        if($fdInfo && $fdInfo['channels'])
        {
            self::delChannel($fd,array_keys($fdInfo['channels']));
        }

        //删除
        self::$redis->hDel(self::FD_PREFIX,$fd);

        return;
    }


    /**
     * ------------------------------------------------------------------
     * 获取所有的连接
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getAllFd()
    {
        return self::$redis->hGetAll(self::FD_PREFIX);
    }

    /**
     * 将用户添加到订阅
     * @param $fd
     * @param $channel
     * @return bool
     * @author soosoogoo
     */
    public static function addChannel($fd,$channel='ALL')
    {
        $fdInfo = self::getFd($fd);
        if (empty($fdInfo))
        {
            return false;
        }
        $fdInfo['channels'][$channel] = 1;
        //更新channel
        $channel = self::$redis->hSet(self::getKey(self::CHANNEL_PREFIX,$channel), $fd, time());
        if ($channel)
        {
            self::editFd($fd,$fdInfo);
        }
    }


    /**
     * 获取某个订阅的详细信息
     * @param $channel
     * @return mixed
     * @author soosoogoo
     */
    public static function getChannel($channel='ALL')
    {
        return self::$redis->hGetAll(self::CHANNEL_PREFIX.$channel);
    }


    /**
     * 将用户从订阅种删除
     * @param $fd
     * @param array $channels
     * @author soosoogoo
     */
    public static function delChannel($fd, array $channels)
    {

        $fdInfo = self::getFd($fd);

        foreach($channels as $channel)
        {
            //从通道删除
            self::$redis->hDel(self::getKey(self::CHANNEL_PREFIX,$channel), $fd);


            //修改用户属性
            if (!empty($fdInfo['channels'][$channel]))
            {
                unset($fdInfo['channels'][$channel]);
            }
        }
        self::editFd($fd,$fdInfo);
    }


    /**
     * 批量检测心跳
     * @param int $ntime
     * @author soosoogoo
     */
    public static function checkHeartbeat($ntime = 60)
    {
        //获取所有的用户
        $allFd = self::$redis->hgetall(self::FD_PREFIX);

        foreach($allFd as $fdInfo)
        {
            $fdInfo = $fdInfo ? json_decode($fdInfo,true) : '';
            if(!$fdInfo)
            {
                continue;
            }

            if ( time() - $fdInfo['time'] > $ntime)
            {
                self::delFd($fdInfo['fd']);
            }
        }
    }


    /**
     * 执行心跳操作
     * @param $fd
     * @return bool|mixed
     * @author soosoogoo
     */
    public static function doheartbeat($fd)
    {
        $fdInfo = self::getFd($fd);
        if (empty($fdInfo))
        {
            return false;
        }
        $update['time'] = time();
        return self::editFd($fd,$update);
    }


    /**
     * 检查用户心跳
     * @param $fd
     * @param int $ntime
     * @return bool
     * @author soosoogoo
     */
    public function heartbeat($fd, $ntime = 60)
    {
        $fdInfo = self::getFd($fd);
        if (empty($fdInfo))
        {
            return false;
        }

        if ( time() - $fdInfo['time'] > $ntime)
        {
            self::delFd($fd);
            return false;
        }
        return true;
    }


}