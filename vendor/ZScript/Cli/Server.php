<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2017/12/2
 * Time: 16:44
 * Desc: cli 运行模式
 */

namespace ZScript\Cli;

use Config\Code\Error;

class Server
{
    static $argv_params;
    public $server;
    public $listen;
    public $table;

    /**
     * @param $key
     * @return array
     * ------------------------------------------------------------
     */
    public static function getArgvParams($key)
    {
        if ($key) {
            return self::$argv_params[$key] ?? null;
        }

        return self::$argv_params;
    }

    /**
     * @param array $params
     * ------------------------------------------------------------
     */
    public static function setArgvParams(array $params)
    {
        $count = count($params);
        $argv_params = [];
        for ($i = 2; $i < $count; $i += 2) {
            $argv_params[$params[$i]] = $params[$i + 1] ?? null;
        }

        self::$argv_params = $argv_params;
    }

    /**
     * @desc 开始服务
     * @param $params
     * @throws
     * ------------------------------------------------------------
     */
    public function startServer($params)
    {
        // 外部参数赋值
        Server::setArgvParams($params);

        list($controller, $action) = $params;

        // 路由分配
        Server::router($controller, $action);
    }

    /**
     * 处理用户请求操作
     * @param $controller
     * @param $action
     * @return void
     * @author len
     * ------------------------------------------------------------
     */
    public function router($controller, $action)
    {
        $router = new \ZScript\Router\Cli();
        $router->autoRoute([$controller, $action]);

        try {

            if (!class_exists($router->controllerName())) {
                echo 'have no controller ', $router->controllerName(), PHP_EOL;
                throw new \Exception(Error::NO_ILLEGAL_OPERATION);
            }

            $controllerName = $router->controllerName();
            $class = new $controllerName();
            $method = $router->methodName();

            if (!method_exists($class, $method)) {
                echo 'have no method ', $method, PHP_EOL;
                throw new \Exception(Error::NO_ILLEGAL_OPERATION);
            }

            $affect = $class->$method();
        } catch (\Exception $e) {
            $err_msg = $e->getMessage();

            // 捕获可控异常
            if (is_numeric($err_msg) && $err_msg < 0) {
                $code = $err_msg;
            } else {
                $code = Error::INNER_ERR;
            }
        }

        exit('game over' . PHP_EOL);
    }
}