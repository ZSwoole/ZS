<?php
namespace ZScript\Library;

/**
 * 数据验证
 */
class Validate
{

    /**
     * 验证 EMAIL
     */
    public static function email($pStr)
    {
        return preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/", $pStr);
    }

    /**
     * 验证 数值
     */
    public static function int($pStr, $pMin = false, $pMax = false)
    {
        if (!is_numeric($pStr)) return false;
        if (false !== $pMin && $pStr < $pMin) return false;
        if (false !== $pMax && $pStr > $pMax) return false;
        return true;
    }

    /**
     * 验证 手机
     */
    public static function mo($pStr)
    {
        if (11 != strlen($pStr)) return false;
        return preg_match("/13[0-9]{9}|15[0-9]{9}|18[0-9]{9}|147[0-9]{8}/", $pStr);
    }

    /**
     * 验证 身份证
     */
    public static function identify($pStr)
    {
        $id15 = '/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/';
        $id18 = '/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}/';
        if (preg_match($id15, $pStr)) {
            return TRUE;
        } else if (preg_match($id18, $pStr)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 验证 用户名
     */
    public static function name($pStr)
    {
        return preg_match("/^[0-9a-zA-Z\x80-\xff]+$/", $pStr);
    }

    /**
     * 验证是否为中正数
     * @param $pStr
     * @return int
     */
    public static function plus($pStr)
    {
        return preg_match("/^[0-9.]+$/", $pStr);
    }

    /**
     * 验证 字母数字
     */
    public static function az09($pStr)
    {
        return preg_match("/^[0-9a-zA-Z_]+$/", $pStr);
    }

    /**
     * 危险字符(XSS, 注入)
     */
    public static function safe($pStr)
    {
        return preg_match("/^[0-9a-zA-Z\x80-\xff@_?&=.\-]+$/", $pStr);
    }

    /**
     * sql
     */
    public static function sql_check($str)
    {
        $check = preg_match('/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file
		|outfile/', $str);
        if ($check) {
            return false;
        }
        return true;
    }

    /**
     * 过滤参数
     * @param string $str 接受的参数
     * @return string
     */
    public static function filterWords($str)
    {
        $farr = array(
            // "/\\s+/",
            "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
            "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
            //"/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is"
            //"/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|load_file|outfile|dump/is"
            "/select|insert|update|delete|\'|\/\*|\.\.\/|\.\/|union|load_file|outfile|dump/is"
        );
        $str = preg_replace($farr, '', $str);
        return $str;
    }

    /**
     * 过滤接受的参数或者数组,如$_GET,$_POST
     * @param array|string $arr 接受的参数或者数组
     * @return array|string
     */
    public static function safeInput($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                $arr[$k] = self::filterWords($v);
            }
        } else {
            $arr = self::filterWords($arr);
        }
        return $arr;
    }

}