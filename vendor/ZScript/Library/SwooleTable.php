<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/6
 * Time: 21:25
 */

namespace ZScript\Library;

class SwooleTable
{
    public static $swooleTable;

    public $coin_table;


    public function __construct()
    {

    }

    /**
     * ------------------------------------------------------------------
     * 静态
     * @return SwooleTable
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getSwooleTable()
    {
        //如若已经有缓存
        if(isset(self::$swooleTable))
        {
            return self::$swooleTable;
        }
        self::$swooleTable = new static();
        return self::$swooleTable;
    }

    /**
     * 创建uid->fd共享内存表
     */
    public function createCoinTable()
    {
        $this->coin_table = new \swoole_table(1024);
        $this->coin_table->column('coin', \swoole_table::TYPE_STRING, 10);
        $this->coin_table->column('coin1', \swoole_table::TYPE_STRING, 10);
        $this->coin_table->create();
        return $this;
    }


    /**
     * 创建task内存表
     */
    public function createTaskTable()
    {
        //创建task用的id->pid共享内存表不至于同时超过1024个任务吧
        //$this->tid_pid_table = new \swoole_table(1024);
        //$this->tid_pid_table->column('pid', \swoole_table::TYPE_INT, 8);
        //$this->tid_pid_table->column('des', \swoole_table::TYPE_STRING, 50);
        //$this->tid_pid_table->column('start_time', \swoole_table::TYPE_INT, 8);
        //$this->tid_pid_table->create();

    }

    /**
     * 将fd绑定到uid,uid不能为0
     * @param $fd
     * @param $uid
     * @param bool $isKick 是否踢掉uid上一个的链接
     */
    public function bindWorkCoin($workId, $coin)
    {
        $this->coin_table->set($workId, ['coin' => $coin]);
    }


    /**
     * 将fd绑定到uid,uid不能为0
     * @param $fd
     * @param $uid
     * @param bool $isKick 是否踢掉uid上一个的链接
     */
    public function getWorkCoin($workId)
    {
        return $this->coin_table->get($workId);
    }




}