<?php

/**
 * Class Sencrypt
 *
 * PHP openssl 加密
 */
namespace ZScript\Library;

class Sencrypt
{
    private static $instance;
    private $cipher_algo;
    private $hash_algo;
    private $iv_num_bytes;
    private $format;

    //const FORMAT_RAW = 0;
    const FORMAT_B64 = 1;
    const FORMAT_HEX = 2;

    /**
     * @param string $cipher_algo 选择加密算法  list:  https://secure.php.net/manual/zh/function.openssl-get-cipher-methods.php
     * @param string $hash_algo 密钥算法     list:  https://secure.php.net/manual/zh/function.openssl-get-md-methods.php
     * @param int $fmt
     * @throws
     */
    public function __construct($cipher_algo = 'aes-256-ctr', $hash_algo = 'sha256', $fmt = Sencrypt::FORMAT_HEX)
    {
        $this->cipher_algo = $cipher_algo;
        $this->hash_algo = $hash_algo;
        $this->format = $fmt;

        if (!in_array($cipher_algo, openssl_get_cipher_methods(true)))
        {
            throw new \Exception("Sencrypt:: - cipher_methods error {$cipher_algo}");
        }

        if (!in_array($hash_algo, openssl_get_md_methods(true)))
        {
            throw new \Exception("Sencrypt:: - md_methods error {$hash_algo}");
        }

        $this->iv_num_bytes = openssl_cipher_iv_length($cipher_algo);
    }

    /**
     * static pool
     * @return object
     * @throws
     */
    public static function getInstance()
    {
        if(!isset(self::$instance)){
            self::$instance = new static();
        }
        return self::$instance;
    }



    /**
     * 加密一个字符串
     * @param  string $in  要加密的字符串
     * @param  string $key 密钥
     * @param  int $fmt    data处理格式  1 base 64  2 二进制
     * @return string      加密后的字符串
     */
    public function Encrypt($in, $key, $fmt = null)
    {
        if ($fmt === null)
        {
            $fmt = $this->format;
        }

        // 生成一个随机额外key,用于追加到原始 data
        $iv = openssl_random_pseudo_bytes($this->iv_num_bytes, $isStrongCrypto);
        if (!$isStrongCrypto) {
            throw new \Exception("Encrypt - echo fail");
        }

        // hhash
        $keyhash = openssl_digest($key, $this->hash_algo, true);

        // 生成密文 encrypt
        $opts =  OPENSSL_RAW_DATA;
        $encrypted = openssl_encrypt($in, $this->cipher_algo, $keyhash, $opts, $iv);

        if ($encrypted === false)
        {
            throw new \Exception('Encrypt -  error: ' . openssl_error_string());
        }

        // 拼接
        $res = $iv . $encrypted;

        // 处理
        if ($fmt == Sencrypt::FORMAT_B64)
        {
            $res = base64_encode($res);
        }
        else if ($fmt == Sencrypt::FORMAT_HEX)
        {
            $res = unpack('H*', $res)[1];
        }

        return $res;
    }

    /**
     * 解密一个字符串
     * @param  string $in  要解密的字符串
     * @param  string $key 密钥
     * @param  int $fmt    data处理格式  1 base 64  2 二进制
     * @return string      解开的string
     */
    public function Decrypt($in, $key, $fmt = null)
    {
        if ($fmt === null)
        {
            $fmt = $this->format;
        }

        $raw = $in;

        // 处理字符串
        if ($fmt == Sencrypt::FORMAT_B64)
        {
            $raw = base64_decode($in);
        }
        else if ($fmt == Sencrypt::FORMAT_HEX)
        {
            $raw = pack('H*', $in);
        }

        //如果长度比随机key长度还短
        if (strlen($raw) < $this->iv_num_bytes)
        {
            throw new \Exception('Decrypt - ' .'data len ->' . strlen($raw) . "   iv len -> {$this->iv_num_bytes}");
        }

        // 获取添加的随机key,和真正的密文
        $iv = substr($raw, 0, $this->iv_num_bytes);
        $raw = substr($raw, $this->iv_num_bytes);

        // hash
        $keyhash = openssl_digest($key, $this->hash_algo, true);

        // 解密
        $opts = OPENSSL_RAW_DATA;
        $res = openssl_decrypt($raw, $this->cipher_algo, $keyhash, $opts, $iv);

        if ($res === false)
        {
            throw new \Exception('Encrypt -  error: ' . openssl_error_string());
        }

        return $res;
    }


}
