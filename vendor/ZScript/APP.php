<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/27
 * Time: 17:33
 */

namespace ZScript;

use Config\Code\Error;
use Helper\Helper;
use Library\Exception\ErrorException;
use ZScript\Cli\Server;
use ZScript\Router\Http;

class APP
{
    /**
     * @var \ZScript\Server\Server
     */
    static $server;

    static $cur_log_name = null;

    static $is_task = false;

    static $unique_log_id = '';

    /**
     * 检查参数必填字段,各种验证
     * @param $fileds
     * @throws
     * @return array
     */
    public static function checkParams($fileds)
    {
        $data = APP::getRequest();
        $handler = 'trim';
        $return = array();
        $check = 0;

        foreach ($fileds as $filed) {
            if (strpos($filed, '|')) {
                list($filed, $handler, $check) = explode('|', $filed);
            }

            if (!isset($data[$filed]) || ($check && !call_user_func(array(Helper::class, $handler), $data[$filed]))) {
                throw new ErrorException(Error::PARAMS_ERR);
            }

            //是否仅仅做验证
            if (function_exists($handler)) {
                $value = $handler($data[$filed]);
            } else {
                $value = $check ? $data[$filed] : call_user_func(array(Helper::class, $handler), $data[$filed]);
            }

            if ($value === 0 && !$value) {
                throw new ErrorException(Error::PARAMS_ERR);
            }
            $return[] = $value;
        }

        return $return;
    }

    /**
     * 获取当前的参数
     * @param null $field
     * @return mixed|null
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getRequest($field = null)
    {
        $request_data = Request\Http::getRequest();

        return $field ? ($request_data[$field] ?? null) : $request_data;
    }

    /**
     * 获取Post参数
     * @param null $field
     * @return mixed|null
     * ------------------------------------------------------------------
     */
    public static function getPost($field = null)
    {
        $request_data = Request\Http::getPost();

        return $field ? ($request_data[$field] ?? null) : $request_data;
    }

    /**
     * @desc 获取外部参数
     * @param null $field
     * @return array
     * ------------------------------------------------------------
     */
    public static function getArgv($field = null)
    {
        return Server::getArgvParams($field);
    }

    /**
     * 格式化参数
     * @param $fileds
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function formatParams($fileds)
    {
        $data = APP::getRequest();
        $return = array();
        foreach ($fileds as $filed) {
            if (strpos($filed, '|')) {
                list($filed, $handler) = explode('|', $filed);
                $return[] = $handler($data[$filed]);
            } else {
                $return[] = isset($data[$filed]) ? $data[$filed] : '';
            }
        }

        return $return;
    }

    /**
     * 发送消息
     * @param $fd
     * @param $message
     * @return bool
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function send($fd, $message)
    {
        //检测fd对应的连接是否存在
        if (APP::getServer()->server->exist($fd)) {
            $data = is_array($message) ? json_encode($message) : $message;
            if (!empty($data))
                APP::getServer()->server->push($fd, $data);
        }
        return true;
    }

    /**
     * 获取服务实例
     * @return \ZScript\Server\Server
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getServer()
    {
        return self::$server;
    }

    /**
     * 设置服务器
     * @param $server
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function setServer($server)
    {
        self::$server = $server;
    }

    /**
     * 批量发送消息
     * @param $fds
     * @param $message
     * @return bool
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function broadcast($fds, $message)
    {
        //如果不是数组,则转成数组
        !is_array($fds) && $fds = [$fds];

        foreach ($fds as $fd) {
            //检测fd对应的连接是否存在
            if (APP::getServer()->server->exist($fd)) {
                $data = is_array($message) ? json_encode($message) : $message;
                if (!empty($data))
                    APP::getServer()->server->push($fd, $data);
            } else {
                // todo 未测试主动 关闭客户端连接
                APP::getServer()->server->close($fd, true);
            }
        }

        return true;
    }

    /**
     * 简单的打印日志
     * @param $msg
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function log($msg)
    {
        if (is_array($msg)) $msg = json_encode($msg, JSON_UNESCAPED_UNICODE);

        echo date('Y-m-d H:i:s') . "  >  " . $msg . "\n";
    }

    /**
     * 参数过滤
     * @param $params
     * @param $fileds
     * @return bool
     */
    public static function _formatParams(&$params, $fileds)
    {
        if (!$params || !$fileds) {
            return true;
        }

        foreach ($fileds as $filedName => $filedValue) {
            $filedValue && $params[$filedName] = $filedValue;
        }

        return true;
    }

    /**
     * 返回值查询
     * @param $affect
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function returnHandle($affect)
    {
        if ($affect == Error::INNER_ERR) {
            $return = ['status' => $affect, 'msg' => 'System error', 'data' => ''];
        } else if (is_numeric($affect) && $affect < 0) {
            // 根据错误码 获取对应提示
            $msg = ''; // todo 自定义获取语言包方式  StaticHandler::langCode()->getLangCode($affect, APP::getLang(), APP::getReplace()) ?: '';
            $return = ['status' => $affect, 'msg' => $msg, 'data' => ''];
        } elseif (is_string($affect)) {
            $affect = json_decode($affect);
            if (isset($affect['status']) && $affect['status'] < 0) {
                $return = ['status' => $affect['status'], 'msg' => $affect['msg'], 'data' => ''];
            }
        } elseif (is_array($affect) || (is_int($affect) && $affect > 0)) {
            if (isset($affect['status']) && $affect['status'] < 0) {
                $return = ['status' => $affect['status'], 'msg' => $affect['msg'], 'data' => ''];
            }
        } elseif (is_object($affect)) {
            if (isset($affect->status) && $affect->status < 0) {
                $return = ['status' => $affect->status, 'msg' => $affect->msg, 'data' => ''];
            }
        } elseif (is_bool($affect)) {
            $return = ['status' => 200, 'msg' => '', 'data' => 1];
        }

        if (!isset($return)) {
            $return = ['status' => 200, 'msg' => '', 'data' => $affect];
        }

        $return['microtime'] = Helper::getMillisecond();
        $return = json_encode($return);

        // 判断当前进程
        if (App::isTask()) return;

        Response\Http::getResponse()->end($return);
    }

    /**
     * @return bool
     * ------------------------------------------------------------
     */
    public static function isTask(): bool
    {
        return APP::getServer()->server->taskworker;
    }

    /**
     * @desc 注销 全局静态变量
     * @author len
     *------------------------------------------------------
     */
    public static function unStaticVar()
    {
        // 注销业务全局变量
    }

    /**
     * @desc 获取worker 进程ID
     * @return int|null
     * ------------------------------------------------------------
     */
    public static function getWorkerPid()
    {
        return APP::getServer()->server->worker_pid ?? null;
    }

    /**
     * tcp 返回值查询
     * @param $affect
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function returnTcpHandle($affect)
    {
        if ($affect == Error::INNER_ERR) {
            $return = ['status' => $affect, 'msg' => 'System error', 'data' => ''];
        } else if (is_numeric($affect) && $affect < 0) {
            // 根据错误码 获取对应提示
            $return = ['status' => $affect, 'msg' => '', 'data' => ''];
        } elseif (is_string($affect)) {
            $affect = json_decode($affect);
            if (isset($affect['status']) && $affect['status'] < 0) {
                $return = ['status' => $affect['status'], 'msg' => $affect['msg'], 'data' => ''];
            }
        } elseif (is_array($affect) || (is_int($affect) && $affect > 0)) {
            if (isset($affect['status']) && $affect['status'] < 0) {
                $return = ['status' => $affect['status'], 'msg' => $affect['msg'], 'data' => ''];
            }
        } elseif (is_bool($affect)) {
            $return = ['status' => 200, 'msg' => '', 'data' => ($affect ? 1 : 0)];
        }

        if (!isset($return)) {
            $return = ['status' => 200, 'msg' => '', 'data' => $affect];
        }

        $return['microtime'] = Helper::getMillisecond();
        $return['worker_id'] = APP::getWorkerId();
        $return = json_encode($return);

        // 判断当前进程 为task 不需要回包 给fd
        if (App::isTask()) return;

        // 发包
        $send_result = Response\Tcp::send($return);

        // log ...
    }

    /**
     * @desc 获取worker ID
     * @return int
     */
    public static function getWorkerId()
    {
        return APP::getServer()->server->worker_id ?? null;
    }

    /**
     * @desc 获取当前router
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getLogic()
    {
        $controllerName = explode('\\', Http::controllerName());
        return array(end($controllerName), Http::methodName());
    }
}
