<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2018/12/5
 * Time: 13:30
 * Desc:
 */

namespace ZScript\Pack;


class TcpPack
{
    /**
     * @desc 解包
     * @param $data
     * @return array
     * ------------------------------------------------------------
     */
    public static function unPack($pack_data)
    {
        try {
            $data = json_decode($pack_data, true);
            if ($data) {
                $interface = $data['s'] ?? '';
                $menthod = $data['m'] ?? '';
                $version = $data['v'] ?? '';
                $params = $data['p'] ?? [];
                $mode = $data['md'] ?? [];

                return [$interface, $menthod, $params, $version, $mode];
            }
        } catch (\Exception $e) {
        }

        return [];
    }

    /**
     * @拼装包
     * @param $data
     * @return false|string
     * ------------------------------------------------------------
     */
    public static function pack($data)
    {
        $pack_data = [
            $data['interface'] ?? '',               // interface
            $data['method'] ?? '',                  // action
            $data['version'] ?? '',                 // 版本
            $data['params'] ?? [],                  // 参数
        ];

        return json_encode($pack_data);
    }
}