<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2/3
 * Time: 15:35
 */

namespace ZScript\Pack;


class MessagePack
{
    /**
     * 包装SerevrMessageBody消息
     * @param $type
     * @param $message
     * @param string $func
     * @return string
     */
    public static function pack(array $func = null,$message)
    {
        $data['func'] = $func;
        $data['message'] = $message;
        return $data;
    }


    /**
     * unpack
     * @param $data
     * @return array
     * @author soosoogoo
     */
    public static function unpack($data)
    {
        $func = $data['func'];
        $message = $data['message'];
        return [$func,$message];
    }
}