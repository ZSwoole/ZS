<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2018/1/5
 * Time: 16:25
 */
namespace ZScript\Pack;

class ProcessPack
{
    /**
     * 包装SerevrMessageBody消息
     * @param $type
     * @param $message
     * @param string $func
     * @return string
     */
    public static function pack($params,  $action = null)
    {
        $data['data'] = $params;
        $data['func'] = $action;
        return $data;
    }


    /**
     * unpack
     * @param $data
     * @return array
     * @author soosoogoo
     */
    public static function unpack($data)
    {
        $params = $data['data'];
        $action = $data['func'];
        return [$params,$action];
    }


}