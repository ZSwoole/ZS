<?php
/**
 * Created by PhpStorm.
 * 进程管理
 * User: soosoogoo
 * Date: 2018/1/5
 * Time: 10:36
 */
namespace ZScript\Process;
use ZScript\APP;

abstract class Process
{
    public $process;
    public $worker_id;
    protected $config;
    protected $log;
    protected $token = 0;  

    /**
     * 协程支持
     * @var bool
     */
    protected $coroutine_need = true;

    /**
     * 启动进程
     * Process constructor.
     * @param $name
     * @param $worker_id
     * @param bool $coroutine_need
     */
    public function __construct($name, $worker_id)
    {
        $this->name = $name;
        $this->worker_id = $worker_id;
        $this->process = new \swoole_process([$this, '__start'], false, false);

        //设置队列模式
        //$this->process->useQueue('-2');
        APP::getServer()->server->addProcess($this->process);
    }

    /**
     * ------------------------------------------------------------------
     * 启动协程
     * @param $process
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public function __start($process)
    {
        //异步监听
        $process->signal(SIGTERM, [$this, "__shutDown"]);

        $this->onStart($process);
    }

    /**
     * 此方法继承类实现
     * @param $process
     */
    public abstract function onStart(\swoole_process $process);

    /**
     * 关闭进程
     */
    public function __shutDown()
    {
        $this->onShutDown();
        APP::log("Process:$this->worker_id", get_class($this) . "关闭成功");
        exit();
    }


    /**
     * 此方法继承类实现
     * @return mixed
     * @author soosoogoo
     */
    abstract protected function onShutDown();

}