<?php
/**
 * 进程管理
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2018/1/5
 * Time: 11:37
 */

namespace ZScript\Process;

use ZScript\Pack;
use Config\Main;
use ZScript\APP;

class ProcessManager
{
    /**
     * @var ProcessManager
     */
    protected static $instance;


    protected $atomic;
    protected $map = [];
    public $oneWayFucName = [];

    //dispatcher 触发器
    const PROCESS_RPC = 1;
    const PROCESS_CLIENT = 2;

    /**
     * ProcessManager constructor.
     */
    public function __construct()
    {
        $this->atomic = new \swoole_atomic();
    }

    /**
     * @param $class_name
     * @param bool $needCoroutine
     * @param string $name
     * @return Process
     * @throws \Exception
     */
    public function addProcess($class_name, $name = '')
    {
        //https://wiki.swoole.com/wiki/page/363.html
        $worker_id = Main::$swooleSettins['worker_num'] + Main::$swooleSettins['task_worker_num'] + $this->atomic->get();

        $this->atomic->add(1);

        //去掉命名空间前缀
        $names = explode("\\", $class_name);
        $process = new $class_name(Main::$projectName . "-" . $names[count($names) - 1], $worker_id);

        if (array_key_exists($class_name . $name, $this->map)) {
            throw new \Exception('存在相同类型的进程，需要设置别名');
        }
        $this->map[$class_name . $name] = $process;
        return $process;
    }

    /**
     * @return ProcessManager
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ProcessManager();
        }
        return self::$instance;
    }

    /**
     * @param $class_name
     * @param $name
     * @return array
     * @throws \Exception
     */
    public function getProcess($class_name, $name = '')
    {
        if (!array_key_exists($class_name . $name, $this->map)) {
            throw new \Exception("不存在$class_name 进程");
        }
        return $this->map[$class_name . $name];
    }

    /**
     * @param $workerId
     * @return Process
     */
    public function getProcessFromID($workerId)
    {
        foreach ($this->map as $process) {
            if ($process->worker_id == $workerId) {
                return $process;
            }
        }
        return null;
    }


    /**
     * Set process name.
     * https://wiki.swoole.com/wiki/page/125.html
     * @param string $name
     * @return void
     */
    public static function setProcessName($name)
    {
        //APP::log('PID is '.\getmypid().'-'.$name) ;
        if (function_exists('cli_set_process_title')) {
            @cli_set_process_title($name);
        } else {
            @swoole_set_process_name($name);
        }
    }


    /**
     * 发送给随机进程
     * @param $uns_data
     * @param callback $callStaticFuc
     * @param int $type
     */
    public static function sendToRandomWorker($uns_data, array $callStaticFuc, $type = self::PROCESS_CLIENT)
    {
        $sendData = Pack\ProcessPack::pack($uns_data, $callStaticFuc);

        $worker_num = Main::$swooleSettins['worker_num'];
        $worker_id_list = range(0, $worker_num - 1);
        $worker_id_list = array_diff($worker_id_list, [APP::getWorkerId()]);

        shuffle($worker_id_list);
        $id = array_shift($worker_id_list);

        //查看是否需要rpc投递
        if ($type == self::PROCESS_RPC) {
            //此任务只投递给work,子进程是收不到回调,如果需要特殊处理,则添加在此处
        } else {
            APP::getServer()->server->sendMessage($sendData, $id);
        }
    }

    /**
     * 发送给指定work 进程
     * https://wiki.swoole.com/wiki/page/363.html
     * 此函数可以向任意worker进程或者task进程发送消息。在非主进程和管理进程中可调用。收到消息的进程会触发onPipeMessage事件
     * @param $id
     * @param $uns_data
     * @param callback $callStaticFuc
     */
    public static function sendToWorker($id, $uns_data, array $callStaticFuc)
    {
        $sendData = Pack\ProcessPack::pack($uns_data, $callStaticFuc);

        APP::getServer()->server->sendMessage($sendData, $id);
    }

    /**
     * 发送给指定task 进程
     * https://wiki.swoole.com/wiki/page/363.html
     * 此函数可以向任意worker进程或者task进程发送消息。在非主进程和管理进程中可调用。收到消息的进程会触发onPipeMessage事件
     * @param $uns_data
     * @param callback $callStaticFuc
     *------------------------------------------------------
     */
    public static function sendToTasker($uns_data, array $callStaticFuc)
    {
        $sendData = Pack\ProcessPack::pack($uns_data, $callStaticFuc);

        APP::getServer()->server->task($sendData);
    }

    /**
     * 创建默认进程
     * @throws
     */
    public function startProcess()
    {
        // 遍历创建 子进程
        if ($process = Main::$pipProcess) {
            foreach ($process as $p) {
                self::addProcess($p);
            }
        }
    }


}