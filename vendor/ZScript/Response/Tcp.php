<?php
/**
 * Created by PhpStorm.
 * User: len
 * Date: 12/5
 * Time: 13:11
 */

namespace ZScript\Response;


use ZScript\APP;

class Tcp
{
    /**
     * @var \swoole_http_response
     */
    static $response;

    static $fd;

    /**
     * @desc 响应客户端
     * @param $return
     * @return bool
     * ------------------------------------------------------------------
     */
    public static function send($return)
    {
        return APP::getServer()->server->send(self::$fd, $return);
    }


    /**
     * 销毁当前request的赋值
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function destroy()
    {
        self::$fd = null;
    }

    /**
     * @param mixed $fd
     * ------------------------------------------------------------
     */
    public static function setFd($fd)
    {
        self::$fd = $fd;
    }
}