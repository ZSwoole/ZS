<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/27
 * Time: 19:11
 */

namespace ZScript\Response;


class Http
{
    /**
     * @var \swoole_http_response
     */
    static $response;

    /**
     * @author len
     * @param \swoole_http_response $response
     *------------------------------------------------------
     */
    public static function setResponse(\swoole_http_response $response)
    {
        self::$response = $response;

        //设置跨域
        $response->header("Access-Control-Allow-Origin","*");
        $response->header("Access-Control-Allow-Headers","content-type");
        $response->header("Access-Control-Request-Method","GET,POST");

        if(isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])== 'OPTIONS'){
            $response->end();
        }
    }


    /**
     * 获取request
     * @return \swoole_http_response
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getResponse()
    {
        return self::$response;
    }


    /**
     * 销毁当前request的赋值
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function destroy()
    {
        self::$response = null;
    }
}