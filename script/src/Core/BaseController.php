<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2018/12/10
 * Time: 5:28 PM
 */

namespace Core;


use Config\Overall\RedisKeys;
use Library\Core\Links;
use ZScript\Process\ProcessManager;

abstract class BaseController
{

    abstract public function init();

}