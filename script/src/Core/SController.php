<?php

namespace Core;

use ZScript\APP;

class SController
{
    /**
     * SController constructor.
     * @throws
     */
    public function __construct()
    {
    }

    public function __destruct()
    {
        // 注销 全局变量
        APP::unStaticVar();
    }
}

