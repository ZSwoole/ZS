<?php

namespace Config\Code;

class Error
{
    //成功
    const CODE_SUCCESS = 200;

    //返回失败
    const RETURNCODE_WRONG = -99;

    //参数错误
    const PARAMS_ERR = -100;

    //非法操作 (会造成前端退出登录操作 APP web)
    const NO_ILLEGAL_OPERATION = -101;

    //签名验证信息不正确
    const RETURNCODE_KEY_NO_VERIFY = -102;

    //参数格式不正确
    const PARAMS_FORMAT_ERR = -103;

    //服务器繁忙
    const SERVER_IS_BUSY = -104;

    //签名错误
    const SIGN_ERROR = -105;

    //请输入来源信息
    const SOURCE_ERROR = -106;

    // 资产操作失败
    const ASSETS_ERR = -599;

    //内部执行错误
    const INNER_ERR = -500;

}