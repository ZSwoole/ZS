<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/29
 * Time: 15:26
 */

namespace Config;


class Databases
{
    public static $redis = array(

        //本身逻辑相关
        'main' => array(
            'host' => '127.0.0.1',
            'port' => '6379',
            'database' => 0,
            'prefix' => ''
        ),
    );

    #mysql配置
    public static $mysql = array(
        'main' => array(
            'host' => '127.0.0.1',
            'username' => 'user',
            'password' => 'pwd',
            'db' => 'user',
            'port' => 3306,
            'charset' => 'utf8',
        ),
    );
}