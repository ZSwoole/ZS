<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 4/27
 * Time: 17:51
 */

namespace Config;

use Listen\Http;

class Main
{
    const TYPE_TCP = 'Tcp';
    const TYPE_HTTP = 'Http';
    const TYPE_WEBSOCKET = 'WebSocket';

    //默认监听协议
    public static $mainMode = self::TYPE_HTTP;

    //listen类设置
    public static $listenMode = Http::class;

    //项目名称
    public static $projectName = 'ZScript';

    //项目注释
    public static $projectExplain;

    //默认子进程
    public static $pipProcess = [];

    //逻辑业务入口
    public static $autoRoute;

    //逻辑业务配置
    public static $serviceConfig;

    //分配任务
    public static $dispense;

    //swooleserver 默认启动参数  会由 BinConfig 目录下覆盖
    public static $swooleSettins = array(
        'host' => '0.0.0.0',                            // socket 监听ip
        'port' => 8001,                                 // socket 监听端口.
        'adapter' => 'Swoole',                          // socket 驱动模块
        'daemonize' => false,                           // 是否开启守护进程
        'server_type' => self::TYPE_HTTP,
        'work_mode' => SWOOLE_PROCESS,                  // 1,base  2 线程  3 进程
        'worker_num' => 1,
        'task_worker_num' => 10,

        'pid_file' => LOG_PATH . DS . 'pid',
        'log_file' => LOG_PATH . DS . 'log',

        'buffer_output_size' => 32 * 1024 * 1024,       // 32M
        'socket_buffer_size' => 32 * 1024 * 1024,

        'enable_coroutine' => false,                    // 是否开启自动协程
        'max_coroutine' => 3000,                        // 最大可创建协程数量 https://wiki.swoole.com/wiki/page/950.html

        'max_request' => 1000,                          // 将请求数改为1，实现热加载，开发环境使用

        //心跳相关
        'heartbeat_idle_time' => 60,
        'heartbeat_check_interval' => 10,
    );

    /**
     * ------------------------------------------------------------------
     * 获取项目名称
     * @return string
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function projectName()
    {
        return self::$projectName;
    }

    /**
     * ------------------------------------------------------------------
     * 获取业务配置
     * @return mixed
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function serviceConfig()
    {
        return self::$serviceConfig;
    }

    /**
     * ------------------------------------------------------------------
     * 任务分发函数
     * @return \Jobs\AutoDispense
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function dispense()
    {
        if (!class_exists(self::$dispense)) {
            return null;
        }

        return new self::$dispense;
    }

    /**
     * ------------------------------------------------------------------
     * 获取默认入口
     * @return \Core\BaseController|null
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function autoRoute()
    {
        if (!class_exists(self::$autoRoute)) {
            return null;
        }

        return new self::$autoRoute;
    }
}