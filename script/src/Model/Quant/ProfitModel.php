<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2019/3/1
 * Time: 6:54 PM
 */
namespace Model\Quant;

use \Library\Client\Curl;
use Library\Core\Links;
use Library\ProfitApi\ProfitApi;

class ProfitModel
{

    const COLLECTION_NAME = 'quant-assets';
    const PLATFORM_URL = 'https://waeye.10bi.com/api-tmp/ApiAgent/Trade/getProfitLossInfo?huobi_point=0&bnb_to_rate=0';

    /**
     * ------------------------------------------------------------------
     * 保存币种盈亏情况
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function coinProfit()
    {
        $coinProfit = ProfitApi::getCoinProfitList();
        Links::getMainMongo(self::COLLECTION_NAME)->deleteMany(array('coin' => array('$gt' => 1)));
        Links::getMainMongo(self::COLLECTION_NAME)->insertMany($coinProfit);
    }

    /**
     * ------------------------------------------------------------------
     * 根据交易计算盈亏
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function tradeProfit()
    {

    }








}