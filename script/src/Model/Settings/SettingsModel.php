<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2019/3/1
 * Time: 10:00 PM
 */
namespace Model\Settings;

use Library\Core\Links;

class SettingsModel
{
    const PUBLIC_MIN_TIMESTAMP = 'public_min_timestamp';

    public static function cacheTimestamp()
    {
        $timestamp =  time() % 60 > 0 ? time() - time()%60 + 60 : time();

        Links::getMainRedis()->set(self::PUBLIC_MIN_TIMESTAMP,$timestamp);
    }


    /**
     * ------------------------------------------------------------------
     * 获取时间戳
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function minTS()
    {
        return Links::getMainRedis()->get(self::PUBLIC_MIN_TIMESTAMP);
    }
}