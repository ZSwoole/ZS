<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2018/12/13
 * Time: 13:05
 * Desc: 通用日志类
 */

namespace Library;

use ZScript\APP;

/**
 * @method static pudanInfo(array $array)
 * @method static spiderMan(string $string)
 * @method static err($string)
 * @method static debug($result)
 */
class Logger
{
    const SPLIT_CHAR = '  ';

    private static $unique_log_id = '|';

    private static $log_name = null;

    /**
     * @desc 魔术方法静态方法不存在下调用
     * @param $name
     * @param $arguments
     * ------------------------------------------------------------
     */
    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __call() method.
        self::general(current($arguments), $name);
    }

    /**
     * @desc 交易对日志
     * @param $msg
     * @param string $log_tag
     * ------------------------------------------------------------
     */
    public static function general($msg, $log_tag)
    {
        $str = '';
        if (is_array($msg)) {
            foreach ($msg as $key => $val) {
                if (is_array($val) || is_object($val))
                    $val = json_encode($val);
                $str .= " {$key} {$val}";
            }
        } else {
            $str .= $msg;
        }

        $worker_id = APP::getWorkerId();
        if (is_null($worker_id)) {
            $worker_id = 'task';
        }
        $date = date('Y/m/d H:i:s');
        $no = date('Ymd');

        $name = self::getLogName();
        if (!$name) {
            $file_name = LOG_PATH . DS . 'loger_' . $no . '.log';
        } else {
            $file_name = LOG_PATH . DS . $name . '_' . $no . '.log';
        }

        $data = $date . self::SPLIT_CHAR;

        // 当前日志唯一标识
        $data .= Logger::getUniqueLogId() . self::SPLIT_CHAR;

        // 当前worker 进程PID
        $data .= APP::getWorkerPid() . self::SPLIT_CHAR;

        // worker id
        $data .= $worker_id . self::SPLIT_CHAR;

        // 标记
        $data .= ucfirst($log_tag) . self::SPLIT_CHAR . $str . " \n";

        file_put_contents($file_name, $data, FILE_APPEND);
    }

    /**
     * @return string
     */
    public static function getLogName()
    {
        return self::$log_name;
    }

    /**
     * @desc
     * @author len
     * @param string $log_name
     * ---------------------------------------------------
     */
    public static function setLogName(string $log_name)
    {
        self::$log_name = $log_name;
    }

    /**
     * @return string
     */
    public static function getUniqueLogId()
    {
        return self::$unique_log_id;
    }

    /**
     * @param string $unique_log_id
     */
    public static function setUniqueLogId(string $unique_log_id)
    {
        self::$unique_log_id = $unique_log_id;
    }

    /**
     * @desc 停止输出
     * @param mixed ...$params
     * ------------------------------------------------------------
     */
    public static function url(...$params)
    {
    }

    /**
     * @desc sdk 错误日志
     * @param $params
     * ------------------------------------------------------------
     */
    public static function sdkErr($params)
    {
        self::log($params, 'SdkErr');
    }

    /**
     * @param $msg
     * @param string $name
     * ------------------------------------------------------------
     */
    public static function log($msg, $name = 'INFO')
    {
        $str = '';
        if (is_array($msg)) {
            foreach ($msg as $key => $val) {
                if (is_array($val))
                    $val = json_encode($val);
                $str .= " {$key} {$val}";
            }
        } else {
            $str .= $msg;
        }

        $date = date('Y/m/d H:i:s');
        $no = date('Ymd');
        $file_name = LOG_PATH . DS . $name . '_' . $no . '.log';
        $data = $date . self::SPLIT_CHAR;

        $worker_id = APP::getWorkerId();
        if (is_null($worker_id)) {
            $worker_id = 'task';
        }

        // 获取唯一标识
        $data .= Logger::getUniqueLogId() . self::SPLIT_CHAR;

        // 当前worker 进程PID
        $data .= APP::getWorkerPid() . self::SPLIT_CHAR;

        $data .= $worker_id . self::SPLIT_CHAR . $str . " \n";

        echo $data . PHP_EOL;

        file_put_contents($file_name, $data, FILE_APPEND);
    }
}