<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/7
 * Time: 17:59
 */

namespace Library\Exception;

use ZScript\APP;

class ErrorException extends \Exception
{
    /**
     * ErrorException constructor.
     * @param $message
     * @param int $code
     * @param \Exception|null $previous
     * @throws
     */
    public function __construct($message, $code = -1, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);

        APP::returnHandle($message);
        return;
    }
}