<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 7/5
 * Time: 21:18
 */

namespace Library\Core;

use Config\Overall\Settings;
use ZScript\DB\Links as ZLinks;


class Links
{

    /**
     * 获取主配置信息
     * @return \Redis
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getMainRedis()
    {
        return ZLinks::getRedis();
    }

    /**
     * 获取mysql连接
     * @return \ZScript\DB\MysqliDb
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getMainMysql()
    {
        return ZLinks::getMysql(Settings::MAIN_WRITE);
    }



    /**
     * 获取主配置信息
     * @return \MongoDB\Collection
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getMainMongo($collection)
    {

        $affect = \ZScript\DB\Links::getMongo('main');

        return $affect::instance($collection);
    }


    /**
     * ------------------------------------------------------------------
     * ODS层数据库
     * @author soosoogoo
     * @return \ZScript\DB\MysqliDb
     * ------------------------------------------------------------------
     */
    public static function getOdsAdsdb()
    {
        return ZLinks::getAds('main');
    }



}
