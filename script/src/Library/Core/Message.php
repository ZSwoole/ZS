<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 5/21
 * Time: 17:50
 */

namespace Library\Core;

use ZScript\APP;

class Message
{

    /**
     * ------------------------------------------------------------------
     * 异步推送消息
     * @param $data
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function doTaksBroadcast($data)
    {
        list($fds, $data) = $data;

        APP::broadcast($fds, $data);
    }
}