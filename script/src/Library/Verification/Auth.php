<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 7/11
 * Time: 2:38
 */
namespace Library\Verification;

class Auth
{


    /**
     * ------------------------------------------------------------------
     * 验证签名
     * @param  $secretKey
     * @return bool
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function signValidate($data,$secretKey)
    {
        if(!$secretKey)
        {
            return false;
        }

        foreach( $data as $k=>$v)
        {
            if(!$v){
                unset($data[$k]);
            }
        }

        $postSign = $data['sign'];

        unset($data['sign']);

        //时间戳兼容
        $data['timeStamp'] = (int)substr($data['timeStamp'],0,10);

        ksort($data);

        $dataStr = '';
        foreach ($data as $k => $v)
        {
            $dataStr .= $k.'='.$v."&";
        }

        $sign = md5(trim($dataStr,"&").$secretKey);

        if($postSign !== $sign)
        {
            return false;
        }

        return true;
    }

}