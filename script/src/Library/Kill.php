<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2019/1/23
 * Time: 19:43
 * Desc: 自杀进程
 */

namespace Library;


use Config\Main;
use ZScript\APP;
use ZScript\Process\ProcessManager;

class Kill
{
    /**
     * @desc 自杀
     * ------------------------------------------------------------
     */
    public static function me()
    {
        //echo 'worker ', APP::getWorkerId(), ' kill -9  ', APP::getWorkerPid(), PHP_EOL;

        // 杀死自己进程
        \swoole_process::kill(APP::getWorkerPid(), SIGKILL);
    }

    /**
     * @desc 重启worker 进程
     * ------------------------------------------------------------
     */
    public static function reloadWorker()
    {
        $workerList = range(0, Main::$swooleSettins['worker_num'] - 1);

        foreach ($workerList as $workerId) {
            ProcessManager::sendToWorker($workerId, [], [Kill::class, 'me']);
        }
    }

}