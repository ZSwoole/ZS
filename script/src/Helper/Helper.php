<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 7/5
 * Time: 21:26
 */

namespace Helper;


class Helper
{
    /**
     * 验证 字母数字
     * @param $string
     * @return false|int
     * ------------------------------------------------------------------
     */
    public static function az09($string)
    {
        return preg_match("/^[0-9a-zA-Z_-]+$/", trim($string));
    }

    /**
     * code json
     * @param $data
     * @return mixed
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function jsonDecode($data)
    {
        return json_decode($data, true);
    }

    /**
     * 解析json
     * @param $data
     * @return string
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function jsonEncode($data)
    {
        return json_encode($data, true);
    }

    /**
     * ------------------------------------------------------------------
     * 获取微妙数
     * @return float
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());

        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

    /**
     * 时间戳 转   日期格式 ： 精确到毫秒，x代表毫秒
     * @param $time
     * @return mixed
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getMicrotimeFormat($time)
    {
        if (strstr($time, '.')) {
            sprintf("%01.3f", $time); //小数点。不足三位补0
            list($usec, $sec) = explode(".", $time);
            $sec = str_pad($sec, 3, "0", STR_PAD_RIGHT); //不足3位。右边补0
        } else {
            $usec = $time;
            $sec = "000";
        }
        $date = date("Y-m-d H:i:s.x", $usec);

        return str_replace('x', $sec, $date);
    }

    /**
     * 时间日期转时间戳格式，精确到毫秒
     * @param $time
     * @return string
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function getDataFormat($time)
    {
        list($usec, $sec) = explode(".", $time);
        $date = strtotime($usec);

        return str_pad($date . $sec, 13, "0", STR_PAD_RIGHT); //不足13位。右边补0
    }

    /**
     * 微妙转秒
     * @param $microtime
     * @return int
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function mtimeToTime($microtime)
    {
        return (int)substr($microtime, 0, 10);
    }

    /**
     * 获取客户端IP
     * @return mixed
     */
    public static function getClientIp()
    {
        $header_data = \ZScript\Request\Http::getHeader();
        $server_data = \ZScript\Request\Http::getServer();
        $tIP = 'unknown';
        foreach (array('x-forwarded-for', 'x-real-forwarded-for', 'x-forwarded-for', 'http_x_forwarded_for', 'x-real-ip', 'http_client_ip', 'remote_addr') as $v1) {
            if (isset($header_data[$v1])) {
                $tIP = current(explode(' ', $header_data[$v1]));
                break;
            }
            if (isset($server_data[$v1])) {
                $tIP = current(explode(' ', $server_data[$v1]));
                break;
            }
        }

        return trim($tIP, ',');
    }

    /**
     * ------------------------------------------------------------------
     * 连接符
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function c($coinFrom, $coinTo)
    {
        return $coinFrom . '_' . $coinTo;
    }

    /**
     * ------------------------------------------------------------------
     * 获取coin bcoin
     * @param $symbol
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------
     */
    public static function e($symbol)
    {
        return explode("_", $symbol);
    }
}
