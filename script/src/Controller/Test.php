<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2019/1/28
 * Time: 22:06
 * Desc:
 */

namespace Controller;

use Core\BaseController;
use Helper\Common;
use Library\Client\Curl;
use Library\Wallet\WalletManager;
use ZScript\APP;

class Test extends BaseController
{
    // worker 进程启动时 执行的任务
    public function init(){}

    const QUANTITY_TAG = "latest";//"latest", "earliest" 或 "pending"
    public static $ethRpc = array(
        "jsonrpc" => "2.0",
        "method" => "personal_listAccounts",
        "params" => "2.0",
        "id" => "64",
    );


    public function getInfo()
    {
        $result = WalletManager::usdtRpc()->getInfo();
        var_dump($result);
    }





}