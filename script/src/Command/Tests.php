<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2019/1/17
 * Time: 11:59
 * Desc: cli 运行模式
 */

namespace Command;


use ZScript\APP;

class Tests
{

    public function run()
    {
        $key = APP::getArgv('key');

        echo 'func ', __FUNCTION__, ' key ', $key, PHP_EOL;
    }

}