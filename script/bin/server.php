<?php
/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/12/2
 * Time: 12:02
 */

use ZScript\ZScript;

//设置时区
\date_default_timezone_set('Asia/Shanghai');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(dirname(__DIR__)));
define('SRC_PATH', dirname(__DIR__) . DS . 'src');
define('BIN_PATH', dirname(__DIR__) . DS . 'bin' . DS);
define('LOG_PATH', BIN_PATH . 'logs' . DS);
define('IS_CLI', false);

include_once SRC_PATH . "/Helper/Helper.php";
include ROOT_PATH . DS . 'vendor' . DS . "autoload.php";
include ROOT_PATH . DS . 'vendor' . DS . 'ZScript' . DS . 'ZScript.php';

ZScript::initWork();

//获取参数
$command = ZScript::commandParser();

/**
 * 启动 配置代参数 --conf-* , * 是bin/conf/下的 配置文件名
 * php /usr/share/nginx/html/bz-quant/script/bin/server.php start --conf-weav_bz_usdt
 */
ZScript::mergeConfig($command);

//输出配置
ZScript::showConfig();

//启动
switch ($command['command']) {
    case "start":
        ZScript::run();
        break;
    case 'stop':
        //        stopServer();
        break;
    case 'reload':
        ZScript::reload();
        break;
}
