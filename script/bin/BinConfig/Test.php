<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2019/1/28
 * Time: 22:04
 * Desc: 测试配置
 */

namespace BinConfig;


class Test
{
    const PROJECT_NAME = 'Test';

    public static $config = array(

        //基础配置
        'projectName' => self::PROJECT_NAME,
        'projectExplain' => '测试',

        //swoole 基础配置
        'swooleSettins' => [
            'port' => 8080,              // socket 监听端口.
            'worker_num' => 1,          // 1个woker 多个process
            'task_worker_num' => 1,
            'task_max_request' => 1,
            'task_ipc_mode' => 3,        // 2, 可定向投递 使用消息队列通信 3 争抢模式
            'max_request' => 1,         //调试专用
            'daemonize' => false,        // 是否开启守护

            'pid_file' => LOG_PATH . DS . self::PROJECT_NAME . '.pid',
            'log_file' => LOG_PATH . DS . self::PROJECT_NAME . '.log',
        ],

        //默认路由 init
        'autoRoute' => \Controller\Test::class,

        //默认任务分发 ,可以不填
        'dispense' => \Jobs\AutoDispense::class,

        //子进程,分配任务
        'pipProcess' => [

            // work 任务 可以指定 任务分发器
            \PipProcess\Dispense::class,

        ],

    );

    /**
     * ------------------------------------------------------------------
     * 项目配置
     * @return array
     * @author soosoogoo
     * ------------------------------------------------------------------\
     */
    public static function config()
    {
        return self::$config;
    }
}