<?php
/**
 * Created by PhpStorm.
 * User: Len
 * Date: 2018/9/10
 * Time: 21:26
 * Desc:
 */

use ZScript\ZScript;

//设置时区
\date_default_timezone_set('Asia/Shanghai');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(dirname(__DIR__)));
define('SRC_PATH', dirname(__DIR__) . DS . 'src');
define('BIN_PATH', dirname(__DIR__) . DS . 'bin' . DS);
define('LOG_PATH', BIN_PATH . 'logs' . DS);
define('IS_CLI', true);

include_once SRC_PATH . "/Helper/Helper.php";
include ROOT_PATH . DS . 'vendor' . DS . 'ZScript' . DS . 'ZScript.php';

// 加载composer
include ROOT_PATH . DS . 'vendor' . DS . 'autoload.php';

ZScript::initWork();

\ZScript\ZScript::cliRun($argv);
