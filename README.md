## 运行模式

### Swoole 运行

````
// 启动  Test配置文件 BinConfig 目录下
php server.php start --conf-Test

// 重启 
php server.php reload --conf-Test

// stop 不推荐
ps -ef |grep -i Test |awk '{print $2}' |xargs kill -9 

````

### Cli 运行

````
// 控制器 方法  key value ....
php cli.php Tests run  key value 

````
## 特性

### 脚本服务

  * 脚本服务  参数`server_type` 仅可使用 HTTP (默认)
  * 高度依赖 Redis (worker进程任务)
    
  框架自带进程应用 `\PipProcess\Dispense::class` 可用于:
 
  * 可自定义分配 worker 进程任务
  * 自定义worker 进程空闲重启时间

```php
        // 分配任务
        \swoole_timer_tick(5000, [self::class, 'saveWorkConfig']);

        // todo 每10分钟 定时重启 可自定义
        \swoole_timer_tick(1000 * 600, [\Library\Kill::class, 'reloadWorker']);
```

### 脚本形式 Worker 进程分配任务

  > 从上面分配任务定时器可以看到子进程启动分配worker 进程任务任务可自定义, 具体操作如下: 
  
```php
<?php

namespace Controller;

use Library\Logger;
use Core\BaseController;

class Test extends BaseController
{
   /**
    *  worker 进程启动时 如果有分配任务 会根据默认路由 调用默认路由的 init 方法
    *  
    *   //默认路由 init
    *  'autoRoute' => \Controller\Test::class,
    */
    public function init()
    {
        // 初始铺单配置
        $pudanInfo = SettingModel::savePudanConfig();

        // 初始化铺单配置
        Logger::pudanInfo(['初始铺单配置 =>' => $pudanInfo]);

        // 每 300MS 更新对标交易对价格
        \swoole_timer_tick(300, [PudanService::class, 'benchmarkPrice']);

        // 设置定时器 更新铺单配置
        \swoole_timer_tick(5000, [SettingModel::class, 'saveConfig']);        
    }

    /**
     * 子进程调用默认路由中 taskDispense 方法 返回值为任务列表
     * ------------------------------------------------------------
     */
    public function taskDispense()
    {
        // 获取需要铺单的交易对
        $pudanList = PudanListModel::pudanList();

        $taskList = [];

        // 分配铺单任务
        foreach ($pudanList as $pudanInfo) {

            if (empty($pudanInfo['benchmark_exchange'])) continue;

            // 任务参数 交易对 sig 标识 交易平台
            $taskList[] = [
                $pudanInfo['symbol'],                                           // 交易对
                $pudanInfo['ac_name'],                                          // sig 账号标识
                $pudanInfo['strategy'],                                         // 策略
                $pudanInfo['timidRate'],                                        // 最怂比例参数 万分比
                $pudanInfo['serviceRate']                                       // 手续费率 万分比
            ];
        }

        // 默认数组key 相当于 分配的worker进程ID
        return $taskList;        
    }
}

```


### 对外接口服务
  
  支持 `Tcp` `Http` `Webscocket` 协议, 框架兼容Swoole4.2版本, `enable_coroutine` 参数为 `flase` 使用 [ go ](https://wiki.swoole.com/wiki/page/p-coroutine.html) 调用协程 (复杂业务不推荐使用swoole协程).
  
  * 自定义参数 [`open_dispatch_func`](https://wiki.swoole.com/wiki/page/698.html) 接口服务开启该参数, 可自定义数据包分发方法 `\Library\Dispatch::mode($packData)`. [分包策略](https://wiki.swoole.com/wiki/page/277.html)
   

### 杂项: 

 * 日志类      `\Library\Logger`
 * 钉钉通知     `\Library\DingTalk\Action`
 * 数据库       `\Library\Core\Links`
 * 通用方法      `\Helper\...`
 * [Saber 人性化的协程HTTP客户端封装库](https://github.com/swlib/saber) `\Swlib\SaberGM` 示例: `\Commond\SpiderMan`
 
 
## 异常总结
1. `PHP Warning: Swoole\Server::start(): onPipeMessage handler error.`

* ProcessManager::sendToWorker($workerId, `$taskInfo`, [Central::class, 'taskRouter']);

* 抛错: `PHP Warning: Swoole\Server::start(): onPipeMessage handler error.`

* 修复 `$taskInfo` 外层包数组 

* ProcessManager::sendToWorker($workerId, `[$taskInfo]`, [Central::class, 'taskRouter']);


 
 